/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */

var uniquePaths = function(m, n) {
    let row = new Array(n).fill(1);
    while (m > 1){
        for (let i = 1; i < n; i++) {
            row[i] = row[i] + row[i-1];
        };
        m--;
    };
    return row[n-1];
}

// Solution 2: DP, Recursion
var memo = new Map();// Memo object to store prefetched results
var uniquePaths = function(m, n) {
		if(memo[m+'_'+n]!==undefined){
			return memo[m+'_'+n];
		}
		if(m===1 || n===1){// base case
			memo[m+'_'+n] = 1;
			return 1;
		}
		memo[m+'_'+n] = uniquePaths(m-1,n) + uniquePaths(m,n-1);//recursion
		return memo[m+'_'+n];
};