/**
 * @param {number[][]} matrix
 * @return {number[][]}
 */
 var transpose = function(matrix) {
    let output = [];
    let arr = [];
    while (matrix[matrix.length-1].length != 0) {
        for (let i of matrix) {
            arr.push(i.shift());
        }
        output.push(arr);
        arr = [];
    }
    return output;
};


// Solution 2:
// var transpose = function(matrix) {
//     let output = Array.from(new Array(matrix[0].length), () => new Array(matrix.length));
//     for (let i = 0; i < matrix.length; i++) {
//         for (let j = 0; j < matrix[0].length; j++) {
//             output[j][i] = matrix[i][j];
//         }
//     }
//     return output
// };