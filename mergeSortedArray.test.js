import { merge } from './mergeSortedArray';

test('testing', () => {
    expect(merge(nums1 = [1, 2, 3, 0, 0, 0], m = 3, nums2 = [2, 5, 10], n = 3)).toBe([1, 2, 2, 3, 5, 10])
})