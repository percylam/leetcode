/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNumber = function(nums) {
    nums.sort();
    console.log(nums)
    for (let i = 0; i < nums.length-1; i+=2) {
        if (nums[i] != nums[i+1]){
            return nums[i]
        }
        // console.log(nums[i]);
        // console.log(nums[i+1]);
    }
    return nums[nums.length-1]
};

console.log(singleNumber(nums=[-336,513,-560,-481,-174,101,-997,40,-527,-784,-283,-336,513,-560,-481,-174,101,-997,40,-527,-784,-283,354]))
// console.log(singleNumber(nums=[1,5,8,1,8]))


//********************************************************************************************************************** */
// Solution 2: Set
 var singleNumber = function(nums) {
    let i = 0;
    let set = new Set();
    while (i < nums.length) {
        if (set.has(nums[i])) {
            set.delete(nums[i]);
        } else {
            set.add(nums[i],i);
        };
        i++;
    };
    let setIter = set.values();
    return setIter.next().value
};

//********************************************************************************************************************** */
// Solution 3: XOR
/** The true table for XOR is as below:
 * 0 ^ 0 = 0
 * 0 ^ 1 = 1
 * 1 ^ 0 = 1
 * 1 ^ 1 = 0
 */
/** 
 * Then we can Bitwise (in binary) to calculate the numbers with XOR.
 * 5 ^ 8 = 13
 * 0101 XOR 1000 = 1101
 * 5 ^ 1 = 4
 * 101 XOR 001 = 100
 */
/**
 * Example arr = [1,2,4,8,2,8,4]
 * 1 ^ 2 ^ 4 ^ 8 ^ 2 ^ 8 ^ 4 = 1
 * So 1 is the output we want.
 */
 var singleNumber = function(nums) {
    let output = 0
    for (let i of nums) {
        output ^= i;
    }
    return output;
};