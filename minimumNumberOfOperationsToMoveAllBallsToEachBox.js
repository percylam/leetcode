// postision 2,4,5 have balls
// to P0: 2+4+5=11
// to P1: (2-1)+(4-1)+(5-1)=8
// ...
// to P5: (2-5)+(4-5)+(5-5)=4

var minOperations = function(boxes) {
    let bp = [];
    let e = 0;
    let output = [];
    for (let i = 0; i < boxes.length; i++) {
        if (boxes.charAt(i) == "1") {
            bp.push(i);
        }
    }
    for (let i = 0; i < boxes.length; i++){
        for (let j of bp) {
            e += Math.abs(i-j);
        }
        output.push(e);
        e = 0;
    }
    return output;
};

console.log(minOperations(boxes = "001011"))