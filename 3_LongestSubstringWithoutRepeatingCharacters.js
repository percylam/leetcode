/**
 * Problem: https://leetcode.com/problems/longest-substring-without-repeating-characters/
 * 
 */

/**
 * @param {string} s
 * @return {number}
 */
 var lengthOfLongestSubstring = function(s) {
     if (s.length < 2) {
         return s.length;
     }
    let output = 0;
    let length = 0;
    let left = 0;
    let right = 0;
    let map = new Map();
    while (right < s.length) {
        let charRight = s.charAt(right);
        if (!map.has(charRight)) {
            right++;
            map.set(charRight,right);
            length = right-left;
            if (length > output) {
                output = length;
            }
        } else {
            length = right-left;
            if (length > output) {
                output = length;
            }
            let charLeft = s.charAt(left);
            map.delete(charLeft);
            left++;
        }
    }
    return output;
};


//*************************************************************************** */
// Solution2:
var lengthOfLongestSubstring = function(s) {
    let map = new Map();
    let output = 0;
    let length = 0;
    let left = 0;
    let right = 0;
    if (s.length < 2) {
        return s.length;
    }
    while (right < s.length) {
        if (!map.has(s.charAt(right))){
            map.set(s.charAt(right), 1);
            length++;
            right++;
        } else {
            if (length > output) {
                output = length;
            }
            map.delete(s.charAt(left))
            length--;
            left++;
        };
    };
    if (length > output) {
        output = length;
    }
    return output;
};


//********************************************************************************************** */
// Solution 3: Two-pointer & Hashmap
var lengthOfLongestSubstring = function(s) {
    let left = 0, right = 0, output = 0, set = new Set();
    while (right < s.length) {
        if (!set.has(s.charAt(right))) {
		// If the charater at next right position doesn't exist in hashmap, that means all the substring are unique. Just move the right pointer to expand the substring and add next right charater to our hashmap.
            set.add(s.charAt(right));
            right++;
        } else {
		// If the charater at next right position is in hashmap already, that means this charater has apperared in between left and right (but we don't know its position). We can move left pointer forward and remove charaters from the hashmap until it has been removed.
            set.delete(s.charAt(left));
            left++;
        };
		// No matter we move the left or right pointer, the substring length will be changed. Thus, we keep recording the longest substring length in every move.
        output = Math.max(output, right - left);
    };
    return output;
};
// TC = O(n)
// SC = O(n)