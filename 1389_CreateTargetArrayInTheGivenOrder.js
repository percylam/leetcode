/**
 * @param {number[]} nums
 * @param {number[]} index
 * @return {number[]}
 */
 var createTargetArray = function(nums, index) {
    let output = [];
    for (let i in index) {
        if (output[index[i]] === undefined) {
            output[index[i]] = nums[i];
        } else {
            let front = output.slice(0, index[i]);
            let end = output.slice(index[i]);
            output = front.concat(nums[i]).concat(end);
        };
    };
    return output;
};