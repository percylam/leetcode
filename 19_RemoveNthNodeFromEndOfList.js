/**
Problem: https://leetcode.com/problems/remove-nth-node-from-end-of-list/
Reference: https://www.youtube.com/watch?v=Kka8VgyFZfc&t=264s
Node: 
Single
*/

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
 var removeNthFromEnd = function(head, n) {
//Step 1: Create a new node (dummy) with value -1;
    let dummy = new ListNode(-1);
// Step 2: Connect dummy node to the head in the very beginning;
    dummy.next = head;
    let slow = dummy;
    let fast = dummy; 
    while (fast.next != null) {
// Step 3: Move the fast node first (the slow node doesn't move in this moment.)
        fast = fast.next;
// Step 4: Create delay with n between Fast Node and Slow Node. 
        if (n <= 0) {
            slow = slow.next;
        }
        n--;
    }
// Step 5: For Singly Linked List, each node stores (1) its node value (head.value) and (2) its next node(head.next) ONLY. Which means we cannot remove the previous node. The steps we have to go though are: 
//  (1) copy value from Next Node to Current Node(we want to remove this node): target.val = target.next.val, then 
//  (2) denote follow nodes: target.next = target.next.next
// Ref: https://www.youtube.com/watch?v=sW8ZaOTtvgI
    slow.next = slow.next.next;
    return dummy.next;
};