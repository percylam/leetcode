/**
 * Problem: https://leetcode.com/problems/sum-of-unique-elements/
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
 var sumOfUnique = function(nums) {
    let output = 0, map = new Map();
    for (let i of nums) {
        if (!map.has(i)) {
            // never appear before
            map.set(i, 0)
            output += i;
        } else {
            let freq = map.get(i);
            if (freq == 0) {
                // appear once
                map.set(i, 1);
                output -= i;
            }
        }
    }
    return output;
};