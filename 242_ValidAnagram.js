/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */

// Map()
// Time Complexity: O(m+n)
// Space Complexity: O(m)
 var isAnagram = function(s, t) {
    let map = new Map();
	// add items to map object from s
    for (let i = 0; i < s.length; i++) {
        if (map.has(s.charAt(i))) {
            let freq = map.get(s.charAt(i));
            freq++;
            map.set(s.charAt(i),freq);
        } else {
            map.set(s.charAt(i),1);
        };
    };
	// remove items from map object with reference to t
    for (let i = 0; i < t.length; i++) {
        if (map.has(t.charAt(i))) {
            let freq = map.get(t.charAt(i));
            if (freq == 1) {
                map.delete(t.charAt(i))
            } else {
                freq--;
                map.set(t.charAt(i),freq);
            };
        } else {
            return false;
        };
    };
	// items in map object should be all removed if it is Anagram.
    return map.size == 0;
};