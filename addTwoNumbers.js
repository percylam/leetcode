/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function (l1, l2) {
    let output = [];
    let carry = 0;
    let length = l1.length <= l2.length ? l1.length : l2.length;
    let longArray = l1.length <= l2.length ? l2 : l1;
    for (let i = 0; i < length; i++) {
        if (i > 0 && l1[i - 1] + l2[i - 1] >= 10) {
            carry = 1;
        } else {
            carry = 0
        }
        output.push((l1[i] + l2[i] + carry) % 10)
    }
    for (let i = length; i < longArray.length; i++) {
        output.push(longArray[i])
    }
    return output
};
console.log(addTwoNumbers([2, 4, 3, 4, 5], [5, 6, 4]))