/**
 * @param {string} s
 * @return {boolean}
 */

// Solution 1: Use Two Pointer & ASCII characters.
// Use Two Pointer.
// further handle: (1) upper & lower case; (2) space and punctuation
/** 
 * ASCII characters
 * 0-9: 48-57
 * A~Z: 65~90;
 * a~z: 97~122;
*/
var isPalindrome = function(s) {
    let left = 0; 
    let right = s.length - 1;
    
    // remove space and punctuation
    while (s.charCodeAt(left) < 48 || s.charCodeAt(left) > 57 && s.charCodeAt(left) < 65 || s.charCodeAt(left) > 90 && s.charCodeAt(left) < 97 || s.charCodeAt(left) > 122) {
        left++;
    };
    while (s.charCodeAt(right) < 48 || s.charCodeAt(right) > 57 && s.charCodeAt(right) < 65 || s.charCodeAt(right) > 90 && s.charCodeAt(right) < 97 || s.charCodeAt(right) > 122) {
        right--;
    };
    
    // Both left & right are Numbers or Alphabet.
    while (left < right) {
        // Numbers
        if (s.charCodeAt(left) > 47 && s.charCodeAt(left) < 58 && s.charCodeAt(right) > s.charCodeAt(right))
        if (s.charCodeAt(left) < 48 || s.charCodeAt(left) > 57 && s.charCodeAt(left) < 65 || s.charCodeAt(left) > 90 && s.charCodeAt(left) < 97 || s.charCodeAt(left) > 122) {
            left++;
        } else if (s.charCodeAt(right) < 48 || s.charCodeAt(right) > 57 && s.charCodeAt(right) < 65 || s.charCodeAt(right) > 90 && s.charCodeAt(right) < 97 || s.charCodeAt(right) > 122) {
            right--;
        } else if (s.charCodeAt(left) === s.charCodeAt(right) || Math.abs(s.charCodeAt(left) - s.charCodeAt(right)) === 32 && s.charCodeAt(left) > 64 && s.charCodeAt(right) > 64) {
            left++;
            right--;
        } else {
            return false;
        };
    };
    return true;
};


// Solution 2: 
const ALPHA_NUM = /^[a-zA-Z0-9]$/;
function isPalindrome(s) {
    let left = 0;
    let right = s.length - 1;
    while (left < right) {
        while (left < right && !ALPHA_NUM.test(s[left])) {
            left++;
        };
        while (left < right && !ALPHA_NUM.test(s[right])) {
            right--;
        };
        if (s[left].toLowerCase() !== s[right].toLowerCase()) {
            return false;
        };
        left++;
        right--;
    };
    return true;
};



// Expected Output: "0P" => false;
// Expected Output: ",." => true;
// Expected Output: " " => true;
// Expected Output: "12" => true;
// Expected Output: "1" => true;
// Expected Output: "a" => true;

