var mySqrt = function(x) {
    if (x == 0) {
        return x
    }
    let i = Math.floor(x / 2);
    while (i ** 2 > x) {
        i = Math.floor(i / 2)
    }
    while (i ** 2 < x) {
        i++
    }
    if (i ** 2 > x) {
        return i-1
    } else {
        return i
    }
};

console.log(mySqrt(x=1000))