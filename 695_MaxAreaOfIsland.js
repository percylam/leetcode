/**
 * @param {number[][]} grid
 * @return {number}
 */

// Problem: https://leetcode.com/problems/max-area-of-island/

// 1. We use DFS to recursively loop into the island to check each box's up, down, left and right direction. The total value is the area of this island.
// 2. We need set the box value to -1 when we visit. Then each box can have -1(visited), 0(not visited) and 1(not visited) possible values.
// 3. Finally, we use Math.max(area, maxArea) to filter out the max area of the grid.
// 4. We will loop through all the grids from the left-up corner to the right-bottom corner.

var maxAreaOfIsland = function(grid) {
    if (grid.length == 0) {
        return 0;
    };
    
    let dfs = (grid, row, col) => {
        let sum = 1;
        grid[row][col] = -1;
        if (row - 1 >= 0 && grid[row - 1][col] == 1) {
            sum += dfs(grid, row-1, col);
        };
        if (row + 1 < grid.length && grid[row+1][col] == 1) {
            sum += dfs(grid, row+1, col);
        };
        if (col - 1 >= 0 && grid[row][col - 1] == 1) {
            sum += dfs(grid, row, col-1);
        };
        if (col + 1 < grid[row].length && grid[row][col + 1] == 1) {
            sum += dfs(grid, row, col+1);
        };
        return sum;
    };
    
    let maxArea = 0;
    for (let row = 0; row < grid.length; row++) {
        let col = grid[row].findIndex((e) => e==1);
        while (col != -1) {
            if (grid[row][col] == 1) {
                const area = dfs(grid, row, col);
                maxArea = area > maxArea ? area : maxArea;
            }
            col = grid[row].findIndex((e) => e==1);
        }
    }
    return maxArea;
};