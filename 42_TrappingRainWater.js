/**
 * @param {number[]} height
 * @return {number}
 */

// Solution 1a:
// Time complexity: O(2n)
var trap = function(height) {
    let barSum = 0;
    let barMax = 0;
    
    // from left to right
    let leftSum = 0;
    let leftMax = 0;
    for (let i = 0; i < height.length; i++) {
        barSum += height[i];
        if (height[i] > barMax) {
            barMax = height[i];
        };
        if (height[i] >= leftMax) {
            leftMax = height[i];
        } else {
            leftSum += (leftMax - height[i]);
        };
    };
    
    // from right to left
    let rightSum = 0;
    let rightMax = 0;
    for (let i = height.length - 1; i > -1; i--) {
        if (height[i] >= rightMax) {
            rightMax = height[i];
        } else {
            rightSum += (rightMax - height[i]);
        };
    };
    let output = leftSum + rightSum + barSum - barMax * height.length;
    return output;
};



//============================================================================================================

// Solution 1b:
// Time complexity: O(n)
/**
 * @param {number[]} height
 * @return {number}
 */

// leftSum + rightSum + bars - whole
var trap = function(height) {
    let barSum = 0;
    // for wholeSum 
    let barMax = 0;
    // from left to right
    let leftSum = 0, leftMax = 0;
    // from right to left
    let rightSum = 0, rightMax = 0;
    
    for (let i = 0; i < height.length; i++) {
        // barSum
        barSum += height[i];
        // wholeSum 
        if (height[i] > barMax) {
            barMax = height[i];
        };
        // leftSum
        if (height[i] >= leftMax) {
            leftMax = height[i];
        } else {
            leftSum += (leftMax - height[i]);
        };
        // rightSum
        if (height[height.length - 1 - i] >= rightMax) {
            rightMax = height[height.length - 1 - i];
        } else {
            rightSum += (rightMax - height[height.length - 1 - i]);
        };
    };
    return (leftSum + rightSum + barSum - barMax * height.length);
};


//============================================================================================================

// Solution 2: Two Pointers
var trap = function(heights) {
	if (heights === null || heights.length === 0) {
		return 0;
	}
    let trapped = 0, low = 0, high = heights.length-1;
    let leftMax = 0, rightMax = 0;
    while (low < high) {
        if (heights[low] <= heights[high]) { 
            // we know that there is wall in the right that is either equal to or higher than our tallest wall in the left.
            // that's why our index has moved so far and we are in this conditional statement
            if (leftMax > heights[low]) {
                trapped += leftMax - heights[low];
            } else {
                leftMax = heights[low];
            };
            low++;
        } else {
            // we know that there is wall in the left that is either equal to or higher than our tallest wall in the right
            // that's why our index has moved so far and we are in this conditional statement
            if (rightMax > heights[high]) {
                trapped += rightMax - heights[high];
            } else {
                rightMax = heights[high];
            };
            high--;
        }
    }
    return trapped;
    // Time Complexity: O(n)
    // Space Complexity: O(1)
}