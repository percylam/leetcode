/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */

/**
 * Problem: https://leetcode.com/problems/search-in-rotated-sorted-array/
 * Ref: https://www.youtube.com/watch?v=U8XENwh8Oy8
 */ 

// You must write an algorithm with O(log n) runtime complexity.
// Use BS
var search = function (nums, target) {
    let l = 0, r = nums.length - 1;
    while (l < r) {
        let m = Math.floor((l + r) / 2);
        if (target == nums[m]) {
            return m;
        } else {
            if (nums[m] >= nums[l]) {
                // portion before m is sorted
                if (target < nums[m] && target >= nums[l]) {
                    // target before m
                    r = m - 1;
                } else {
                    // target after m
                    l = m + 1;
                };
            } else {
                // portion after m is sorted
                if (target > nums[m] && target <= nums[r]) {
                    // target after m
                    l = m + 1;
                } else {
                    // target before m
                    r = m - 1;
                };
            };
        };
    };
    return nums[l] == target ? l : -1;
};

// console.log(search(nums = [0,1,2,3,4,5,6,7,8], target = 3)) // Pass
// console.log(search(nums = [4,5,6,7,0,1,2], target = 0)) // Pass
// console.log(search(nums = [3,1], target = 1)) // Pass
// console.log(search(nums = [1], target = 3)) // Pass
// console.log(search(nums = [4,5,6,7,0,1], target = 0)) // Pass
// console.log(search(nums = [1,3,5], target = 1)) // Pass