/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isIsomorphic = function (s, t) {
    if (s.length != t.length) {
        return false;
    }
    let map = new Map();
    let record = new Map();
    for (let i = 0; i < s.length; i++) {
        if (map.get(s.charAt(i)) === undefined) {
            if (record.get(t.charAt(i)) === undefined) {
                map.set(s.charAt(i), t.charAt(i));
                record.set(t.charAt(i), s.charAt(i))
            } else {
                return false
            }
        } else {
            if (map.get(s.charAt(i)) != t.charAt(i)) {
                return false;
            }
        }
    }
    return true
};

console.log(isIsomorphic(s = "badc", t = "baba"))