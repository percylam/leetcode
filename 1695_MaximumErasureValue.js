/**
 * @param {number[]} nums
 * @return {number}
 */
 var maximumUniqueSubarray = function(nums) {
    let output = 0;
    let sum = 0
    let left = 0;
    let right = 0;
    let map = new Map();
    while (right < nums.length) {
        if (!map.has(nums[right])) {
            map.set(nums[right],1);
            sum += nums[right];
            right++;
        } else {
            map.delete(nums[left]);
            output = output > sum ? output : sum;
            sum -= nums[left];
            left++;
        };
    };
    return output = output >= sum ? output : sum;
};