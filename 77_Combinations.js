/**
 * @param {number} n
 * @param {number} k
 * @return {number[][]}
 */

// NOT GOOD APPROACH
// var combine = function (n, k) {
//   let output = [];
//   let item = [];
//   if (n < k) {
//     return output;
//   };
//   let start = n;
//   while (item.length < k) {
//     item.push(start);
//     start--;
//   }
//   while (item[])

//   combine(n-1, k);
// }
// console.log(combine(n = 5, k = 3));

/**
 * Input: n = 5, k = 3
Output:
[
  [5,4,3], [5,4,2], [5,4,1],
  [5,3,2], [5,3,1],
  [5,2,1]
]
 */


const combine = function (n, k) {
  const ans = [];

  const BT = (n, k, stack, idx, ans) => {
    // console.log('stack=',stack);
    // console.log('idx=',idx);
    if (stack.length === k) {
      ans.push([...stack]); // why must use ...stack?
      return;
    }
    for (let i = idx; i < n + 1; i++) {
      stack.push(i);
      BT(n, k, stack, i + 1, ans)
      stack.pop();
    }
  }

  BT(n, k, [], 1, ans);

  return ans;
};
console.log(combine(n = 5, k = 3));
