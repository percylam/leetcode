// Solution 1: Stack
var isValid = function (s) {
    let expectClose = [];
    for (let i = 0; i < s.length; i++) {
        if (s[i] == "{") {
            expectClose.unshift("}");
        } else if (s[i] == "[") {
            expectClose.unshift("]");
        } else if (s[i] == "(") {
            expectClose.unshift(")");
        } else {
            if (s[i] == expectClose[0]) {
                expectClose.shift();
            } else {
                return false;
            };
        };
    };
    return expectClose.length == 0;
};

console.log(isValid(s = "{[]}"))


// Solution 2: Stack
var isValid = function(s) {
    let stack = [];
    for (let i = 0; i < s.length; i++) {
        let a = s.charAt(i);
        if (a === "(" || a === "[" || a === "{") {
            stack.push(a);
        } else {
            let b = stack.pop();
            if (b === "(" &&  a === ")") {
                continue;
            } else if (b === "[" &&  a === "]") {
                continue;
            } else if (b === "{" &&  a === "}") {
                continue;
            } else {
                return false;
            };
        };
    };
    return stack.length == 0;
};