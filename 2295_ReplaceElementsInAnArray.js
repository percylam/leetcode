/**
 * @param {number[]} nums
 * @param {number[][]} operations
 * @return {number[]}
 */
 var arrayChange = function(nums, operations) {
    for (let i of operations) {
        let index = nums.indexOf(i[0]);
        nums[index] = i[1];
    };
    return nums;
};


// Solution 2: hashtable.
var arrayChange = function(nums, operations) {
    let map = new Map();
    for (let i in nums) {
        // map = {value:index}
        map.set(nums[i],i);
    };
    for (let i of operations) {
        let index = map.get(i[0]);
        nums[index] = i[1];
        // renew the hashtable
        // map.delete(i[0]); // 可省
        map.set(i[1],index);
    }
    return nums;
};