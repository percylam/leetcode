/**
 * @param {number[]} nums
 * @return {number}
 */
 var findMaxConsecutiveOnes = function(nums) {
    let res = 0;
    let len = 0;
    for (let i of nums) {
        if (i == 1) {
            len++;
        } else {
            res = Math.max(res, len);
            len = 0;
        }
    }
    res = Math.max(res, len);
    return res;
};