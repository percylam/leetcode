// Step 1. Reverse the entire array;
// Step 2. Reverse the front part of the array;
// Step 3. Reverse the tail part of the array;

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
 var rotate = function(nums, k) {
    k = k % nums.length;
    if (nums.length == 1 || k == 0) {
        return;
    };
    const reverse = (arr, start, end) => {
        while (start < end) {
            let temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }
    nums.reverse();
    //[7,6,5,4,3,2,1]
    // k = 3; nums.length - k = 4
    reverse(nums,0,k-1);
    reverse(nums,k,nums.length-1);    
};