/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */

// head.val = 1
// head.val.next = 2
// head.next = [2,3,4,5]
// Therefore, we can use head.next to determin the start point of the link list. eg: head = head.next and return head, then we will obtain [2,3,4,5].

var middleNode = function (head) {
    var length = 0;
    let currentNode = head;
    while (currentNode.next !== null) {
        length++;
        currentNode = currentNode.next;
    };
    for (let i = 0; i < length / 2; i++) {
        // output = head.next;
        head = head.next;
    }
    return head
};


/************************************************************* */
// Solution 2: 1-way travel without knowing the length.
var middleNode = function (head) {
    let fast = head;
    let slow = head;
    let fastLength = 0;
    let slowLength = 0;
    while (fast.next !== null) {
        fast = fast.next;
        fastLength += 1;
        if (fastLength / slowLength > 2) {
            slow = slow.next;
            slowLength += 1;
        }
    }
    return slow
};


/************************************************************* */
// Solution 3: 1-way travel.
var middleNode = function (head) {
    let fast = head;
    let slow = head;
    while (fast !== null && fast.next !== null) {
        slow = slow.next;
        fast = fast.next.next;
    }
    return slow;
};