var kidsWithCandies = function (candies, extraCandies) {
    let output = [];
    let greatest = Math.max(...candies);
    for (let i in candies) {
        if (candies[i] + extraCandies >= greatest) {
            output.push(true)
        } else {
            output.push(false)
        }
    }
    return output
};
console.log(kidsWithCandies(candies = [2, 3, 5, 1, 3], extraCandies = 3))