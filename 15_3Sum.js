/**
 * @param {number[]} nums
 * @return {number[][]}
 */

// Two Pointer
var threeSum = function (nums) {
    nums.sort((a, b) => a - b);
    let output = [], i = 0;
    while (i < nums.length - 2) {
        let l = i + 1, r = nums.length - 1;
        while (l < r) {
            if (nums[l] + nums[r] + nums[i] == 0) {
                output.push([nums[i], nums[l], nums[r]]);
                while (nums[l] == nums[l + 1]) {
                    l++;
                };
                l++
            } else if (nums[l] + nums[r] + nums[i] > 0) {
                r--;
            } else {
                l++;
            };
        };
        while (nums[i] === nums[i + 1]) {
            i++;
        };
        i++;
    };
    return output;
};