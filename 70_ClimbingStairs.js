/**
 * @param {number} n
 * @return {number}
 */

// Problem: https://leetcode.com/problems/climbing-stairs/
// Ref: https://www.youtube.com/watch?v=Y0lT9Fck7qI&t=941s

// 1 1 2 3 5 8
// For n = 5, 
//  Case 0 (We are already at the end.): - When we are in 5th step, we have 1 way to the end.
//  Case 1: - When we are in 4th step, we have 1 way to the end.
//  Case 2: - When we are in 3rd step, we have 1+1=2 way to the end.
//  Case 3: - When we are in 2nd step, we have 1+2=3 way to the end.
//  Case 4: - When we are in 1st step, we have 2+3=5 way to the end.
//  Case 5: - When we are in 0 step, we have 3+5=8 way to the end.

 var climbStairs = function(n) {
    if (n < 2) {
        return n;
    }
    let output = 1;
    let previous = 1
    while (n > 1) {
        let temp = output;
        output += previous;
        previous = temp;
        n--;
    }
    return output;
};
