var numJewelsInStones = function(jewels, stones) {
    let jewelsArr = jewels.split("");
    let stonesArr = stones.split("");
    let output = 0;
    for (let j of jewels) {
        for (let s of stones) {
            if (j == s){
                output += 1
            }
        }
    }
    return output
};

console.log(numJewelsInStones(jewels="aA",stones="aAAbbb"))