// Question: 
// Input: x = 123
// Output: 321
// Input: x = -123
// Output: -321
// Input: x = 120
// Output: 21
// Input: x = 0
// Output: 0
// (Constraints:)
// -231 <= x <= 231 - 1

var reverse = function(x) {
    let xString = x.toString();
    let xArray = xString.split("");
    let xRevArray = xArray.reverse();
    let xRevString = xRevArray.join("");
    let xRev = parseInt(xRevString,10);
    if (x <= (2**31-1) && x >= ((-2)**31) && xRev <= (2**31-1) && xRev >= ((-2)**31)){
        if (x >= 0){
            return xRev
        } else {
            return (xRev * (-1))
        }
    } else {
        return 0
    }
};