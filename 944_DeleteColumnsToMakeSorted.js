/**
 * @param {string[]} strs
 * @return {number}
 */
var minDeletionSize = function (strs) {
    let output = 0, col = 0;
    while (col < strs[0].length) {
        let prev = strs[0].charCodeAt(col), row = 1;
        while (row < strs.length) {
            let curr = strs[row].charCodeAt(col);
            if (prev > curr) {
                output++;
                break;
            } else {
                prev = curr;
                row++;
            }
        }
        col++;
    }
    return output;
};


console.log(minDeletionSize(strs = ["rrjk", "furt", "guzm"]))