/**
 * @param {number[]} nums
 * @return {number}
 */

// Use Median instead of Mean.(single direction traversal)
var minMoves2 = function(nums) {
    // Cannot use nums.sort() since these are negative numbers.
    nums.sort((a,b) => a-b);
    let medIndex = Math.floor(nums.length / 2);
    let med = nums[medIndex];
    let output = 0;
    for (let i of nums) {
        output += Math.abs(i - med);
    };
    return output;
};


// Solution 2: 
// Use Median instead of Mean (2-direction traversal).
var minMoves2 = function(nums) {
    nums.sort((a,b) => a-b);
    let medIndex = Math.floor(nums.length / 2);
    let med = nums[medIndex];
    let output = 0;
    let i = 0;
    while (i < medIndex) {
        output += (med - nums[i]);
        output += (nums[nums.length - 1 - i] - med);
        i++;
    }
    return output;
};
