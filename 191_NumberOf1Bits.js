/**
 * @param {number} n - a positive integer
 * @return {number}
 */

// Problem: https://leetcode.com/problems/number-of-1-bits/
// Ref: https://www.youtube.com/watch?v=5Km3utixwZs

var hammingWeight = function (n) {
    let output = 0;
    while (n != 0) {
        if (n % 2 == 1) {
            output++;
            // shift right in binary for odd number
            n = (n - 1) / 2;
        } else {
            // shift right in binary even number.
            n /= 2;
        };
    };
    return output;
};