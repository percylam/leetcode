var maximumWealth = function(accounts) {
    let output = 0;
    for (let i = 0; i < accounts.length; i++) {
        let sum = 0;
        accounts[i].forEach (e => {
            sum += e;
        })
        if (sum > output) {
            output = sum
        }
    }
    return output;
};
console.log(maximumWealth(accounts = [[1,2,3],[3,2,1],[10,2,1]]))