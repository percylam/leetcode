var targetIndices = function(nums, target) {
    // How many elements not bigger than target
    let notBiggerThanTarget = nums.filter(e => e<=target);
    let equalToTargetNum = notBiggerThanTarget.filter(e => e==target).length;
    let lessThanTargetNum = notBiggerThanTarget.length - equalToTargetNum;
    let output = [];
    for (let i = lessThanTargetNum; i < notBiggerThanTarget.length; i++){
        output.push(i)
    }
    return output
};

console.log(targetIndices(nums=[48,90,9,21,31,35,19,69,29,52,100,54,21,86,6,45,42,5,62,77,15,38],target=6))