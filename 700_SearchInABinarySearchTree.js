/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} val
 * @return {TreeNode}
 */


// Problem: https://leetcode.com/problems/search-in-a-binary-search-tree/
// Recursion (利用Stack存放Parameter，return地址等)
// 1. 終止條件（Base）：當問題足夠小的時候，用來直接解決問題的Base。
// 2. 遞歸關係（Recursion Relation）：定義 大問題 和 小問題 的關係。
var searchBST = function(root, val) {
    // Base
    if (root == null) {
        return null;
    };
    
    // Recursion Relation
    if (val < root.val) {
        return searchBST(root.left, val);
    };
    if (val > root.val) {
        return searchBST(root.right, val);
    };
    
    // Base
    return root
};