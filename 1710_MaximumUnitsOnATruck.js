/**
 * @param {number[][]} boxTypes
 * @param {number} truckSize
 * @return {number}
 */
 var maximumUnits = function(boxTypes, truckSize) {
    let output = 0;
    let map = new Map();
    let boxSizeArr = [];
    for (let i of boxTypes) {
        if (!map.has(i[1])) {
            map.set(i[1], i[0]);
            boxSizeArr.push(i[1]);
        } else {
            let val = map.get(i[1]);
            val += i[0];
            map.set(i[1], val);
        }
    };
    boxSizeArr.sort((a,b) => a - b); // min, ..., max
    while (truckSize > 0 && boxSizeArr.length > 0) {
        let maxBox = boxSizeArr.pop();
        let boxQ = map.get(maxBox);
        if (boxQ < truckSize) {
            output += (maxBox * boxQ);
            truckSize -= boxQ;
        } else {
            return output += (maxBox * truckSize);
        }
    }
    return output;
};