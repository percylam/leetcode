/**
 * @param {number[][]} mat
 * @return {number[][]}
 */


/**
* BFS
*/
var updateMatrix = function(mat) {
    // find zeroes and mark visited as Infinity
	// create queue which is composed of zeroes in the matrix
	// mark 1s as Infinity
    const findZeroes = () => {
        const output = [];
        for (let row = 0; row < mat.length; row++) {
            for (let col = 0; col < mat[0].length; col++) {
                if (mat[row][col] == 0) {
                    output.push([row, col, 0]);
                } else {
                    mat[row][col] = Infinity;
                };
            };
        };
        return output;
    };
    
    const directions = [
        [-1, 0],
        [1, 0],
        [0, -1],
        [0, 1]
    ];
    const queue = findZeroes();
    
    while (queue.length) {
        for (let i = 0; i < queue.length; i++) {
            const [row, col, path] = queue.shift();
            mat[row][col] = Math.min(mat[row][col], path);
            
            // traverse adjacents
            for (let [dx, dy] of directions) {
                const x = row + dx;
                const y = col + dy;
                
                // handle invalids
                if (x < 0 || x >= mat.length) {
                    continue;
                };
                if (y < 0 || y >= mat[0].length) {
                    continue;
                };
                if (mat[x][y] !== Infinity) {
                    continue;
                };
                queue.push([x, y, path + 1]);
            };
        };
    };
    return mat;
};