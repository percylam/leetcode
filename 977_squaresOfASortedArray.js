/**
 * problem: https://leetcode.com/problems/squares-of-a-sorted-array/
 * 
 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var sortedSquares = function(nums) {
    let result = [];
    while (nums.length > 0) {
        let left = nums[0];
        let right = nums[nums.length-1];
        if (Math.abs(left) > Math.abs(right)) {
            result.unshift(Math.pow(left,2));
            nums.shift();
        } else {
            result.unshift(Math.pow(right,2));
            nums.pop();
        }
    }
    return result;
};

// Solution 2: Didn't modify the input array.
// var sortedSquares = function(nums) {
//     let result = [];
//     var left = 0;
//     var right = nums.length - 1;
//     while (left <= right) {
//         if (Math.abs(nums[left]) > Math.abs(nums[right])) {
//             result.unshift(Math.pow(nums[left],2));
//             left++;
//         } else {
//             result.unshift(Math.pow(nums[right],2));
//             right--;
//         }
//     }
//     return result;
// };

// console.log(sortedSquares([-4,-1,0,3,10]));
console.log(sortedSquares([-7,-3,2,3,11]));