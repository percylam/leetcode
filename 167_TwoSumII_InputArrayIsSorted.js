// Problem: https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/

/**
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */

// Solution 1: use Map() to creat hashtable. However, this method didn't well use the given sorted array.
// var twoSum = function (numbers, target) {
//     let map = new Map();
//     for (let i = 0; i < numbers.length; i++) {
//         let needValue = target - numbers[i];
//         if (map.has(needValue)) {
//             let a = map.get(needValue);
//             return output = [a + 1, i + 1];
//         } else {
//             map.set(numbers[i], i)
//         }
//     }
// };


//******************************************************************************************************************* */
// Solution 2: use 2 points (head & tail) to approach the target.
var twoSum = function (numbers, target) {
    let head = 0;
    let tail = numbers.length - 1;
    while (head < tail) {
        if (numbers[head] + numbers[tail] == target) {
            return [head + 1, tail + 1];
        } else if (numbers[head] + numbers[tail] > target) {
            tail--;
        } else {
            head++;
        }
    }
};

console.log(twoSum(numbers = [2, 7, 11, 15], target = 9))