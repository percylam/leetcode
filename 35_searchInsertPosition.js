var searchInsert = function (nums, target) {
    // Dummy Solution:
    // let output = 0;
    // for (let i = 0; i < nums.length; i++) {
    //     if (nums[i] < target) {
    //         output += 1
    //     } else {
    //         return output
    //     }
    // }
    // return output

    // Binary Search Solution:
    let left = 0;
    let right = nums.length - 1;
    while (right > left) {
        let mid = Math.floor((left + right) / 2);
        // console.log('mid=',mid)
        if (nums[mid] == target) {
            return mid;
        } else if (nums[mid] < target) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    // console.log('left=', left)
    // console.log('right=', right)
    if (target > nums[left]) {
        return left + 1;
    } else {
        return left;
    }
};

// console.log(searchInsert(nums=[1,3,5,6], target=5))
// console.log(searchInsert(nums=[1,3,5,6], target=2))
console.log(searchInsert(nums = [1, 3, 5], target = 2))
// console.log(searchInsert(nums=[1,3,5,6], target=7))