/**
 * @param {string} s
 * @return {string}
 */
 var reverseWords = function(s) {
    // s="God Ding"
    let space = s.split(' ');
    // space = ["God","Ding"]
    for(let i in space) {
        let iArr = space[i].split('');
        // iArr = ["G","o","d"]
        let head = 0;
        let tail = iArr.length - 1;
        while (head < tail) {
            let temp = iArr[head];
            iArr[head] = iArr[tail];
            iArr[tail] = temp;
            head++;
            tail--;
        }
        // iArr = ["d","o","G"]
        let iReversed = iArr.join('');
        // iReversed = "doG"
        space[i] = iReversed;
    };
    let result = space.join(' ');
    return result;
};


//************************************************************************** */
//Solution 2: use array.reverse() function
var reverseWords = function(s) {
    // s = "Let's take LeetCode contest"
    let space = s.split(' ');
    // space = ["Let's","take","LeetCode","contest"]
    let output = "";
    for (let i of space) {
        if (output.length > 0) {
            output += " "
        }
        let wordArr = i.split('');
        // wordArr = ["L","e","t","'","s"]
        let revWord = wordArr.reverse().join('');
        // revWord = "s'teL"
        output += revWord; 
    }
    return output
};