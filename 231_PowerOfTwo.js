/**
 * @param {number} n
 * @return {boolean}
 */

// [1,2,4,8,16,32] => True
// [6,10,12,14,18,20] => False
// [3,5,7,9] => False (n % 2 != 0)

var isPowerOfTwo = function (n) {
    if (n < 1) {
        return false;
    };
    while (n % 2 == 0) {
        n /= 2;
    };
    return n == 1 ? true : false;
};


// Solution 2: Bitwise AND (&) (Details: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_AND)

/** Testing:
* let n = 32 (100000) 
* n - 1 = 31 (11111)

* 1 0 0 0 0 0   n
* 0 1 1 1 1 1   n - 1
*-------------- & gate
* 0 0 0 0 0 0   result

*/

var isPowerOfTwo = function (n) {
    return n <= 0 ? false : (n & (n - 1)) == 0;
};