/**
 * @param {number[]} nums
 * @return {number[][]}
 */

// Problem: https://leetcode.com/problems/permutations/
// Ref: https://www.youtube.com/watch?v=4FdPoW4Qzb4&t=403s&ab_channel=AndyGala

var permute = function (nums) {
    var output = [];

    const dfs = (i, nums) => {
        // base case
        if (i == nums.length) {
            output.push(nums.slice());
            return;
        };
        // dfs recursie case
        for (let j = i; j < nums.length; j++) {
            // swap
            [nums[i], nums[j]] = [nums[j], nums[i]];
            dfs(i + 1, nums);
            // swap back
            [nums[i], nums[j]] = [nums[j], nums[i]];
        };
    };

    dfs(0, nums);
    return output;
};

console.log(permute(nums = [1, 2, 3, 4, 5]))