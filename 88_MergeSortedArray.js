/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
    let tail = nums1.length - 1;
    while (m - 1 >= 0 && n - 1 >= 0) {
        if (nums1[m - 1] >= nums2[n - 1]) {
            nums1[tail] = nums1[m - 1];
            m--;
        } else {
            nums1[tail] = nums2[n - 1];
            n--;
        }
        tail--;
    }
    while (n - 1 >= 0) {
        nums1[tail] = nums2[n - 1];
        n--;
        tail--;
    }
};
// Expect: [1,2,2,3,5,6]
// console.log(merge(nums1=[1,2,3,0,0,0],m=3,nums2=[2,5,6],n=3));

// Expect: [1]
// console.log(merge(nums1=[1],m=1,nums2=[],n = 0));

// Expect: [1]
// console.log(merge(nums1=[0],m=0,nums2=[1],n=1));

// Expect: [1,2]
// console.log(merge(nums1=[2,0],m=1,nums2=[1],n=1)); 
