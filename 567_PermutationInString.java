// Since the combination for permutaion are too much. But we noted that all the chareaters are in low case from alphabet table (26 in total). So it is more easy to creat 2 hashmap for s1 and s2. If the alphabet frequency in s1 and s2 are exect the same in the same window length. Then we can confirm s2 contains permutation of s1.

public class Solution {
    public boolean checkInclusion(String s1, String s2) {
        if (s1.length() > s2.length()) {
            return false;
        }
// Initiate 2 number arrays (size = 26), each number represents the frequency of the coresponding alphabet appeared in s1 and s2.
        int[] s1map = new int[26];
        int[] s2map = new int[26];
// loop into s1;
        for (int i = 0; i < s1.length(); i++) {
// Each charater has its own value (ASCII). This method helps to reconize the alphabet. Ref: http://sticksandstones.kstrom.com/appen.html
            s1map[s1.charAt(i) - 'a']++;
            s2map[s2.charAt(i) - 'a']++;
        }
        int count = 0;
// Since there are 26 unique charaters (alphabet), we loop 26 times for that. The index (i) indicate the coresponding alphabet.
        for (int i = 0; i < 26; i++) {
            if(s1map[i] == s2map[i]) {
                count++;
            }
        }
// WHAT DOES THE FOLLOWING PART DO ??
        for (int i = 0; i < s2.length() - s1.length(); i++) {
            int r = s2.charAt(i + s1.length()) - 'a', l = s2.charAt(i) - 'a';
            if (count == 26) {
                return true;
            }
            s2map[r]++;
            if (s2map[r] == s1map[r]) {
                count++;
            } else if (s2map[r] == s1map[r] + 1) {
                count--;
            }
            s2map[l]--;
            if (s2map[l] == s1map[l]) {
                count++;
            } else if (s2map[l] == s1map[l] - 1) {
                count--;
            }
        }
        return count == 26;
    }
}


