/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */

// Problem: https://leetcode.com/problems/reverse-linked-list/

// Solution 1: Iterative
// Ref: https://www.youtube.com/watch?v=N_Y12-5oa-w&ab_channel=FisherCoder
var reverseList = function (head) {
    let prev = null;
    let curr = head;
    while (curr != null) {
        // next (temporary node) only occurs in this while loop. It is to keep the access to the rest of nodes in head.
        let next = curr.next;
        // reverse single node in the linked list;
        curr.next = prev;

        // -----For the next iteration.-----
        // previous node not a null anymore. it will be shifted to current node.
        prev = curr;
        // current node will be shifted to next node as well.
        curr = next;
    };
    return prev;
};


// Solution 2: Recursive
// Ref: https://www.youtube.com/watch?v=W-EfGB0E_ao&ab_channel=FisherCoder
var reverseList = function (head) {
    const reverse = (curr = head, prev = null) => {
        // Base case（終止條件）
        if (curr === null) {
            return prev;
        };
        // Recursion Relation case（遞歸關係）
        const next = curr.next;
        curr.next = prev;
        return reverse(next, curr);
    };
    return reverse();
};