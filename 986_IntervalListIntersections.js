/**
 * @param {number[][]} firstList
 * @param {number[][]} secondList
 * @return {number[][]}
 */

// Problem: https://leetcode.com/problems/interval-list-intersections/
// Ref: https://www.youtube.com/watch?v=PVeDmre3rSc

/** Use 2-pointers.
*   Step 1: Set 1st pointer(p1) at the head of firstList, 2nd pointer(p2) at the head of secondList. Both pointers will travel from left to right in their own lists.
*   Step 2: There are only 2 scenarios (Overlapping OR Not overlapping.).
*/

var intervalIntersection = function (firstList, secondList) {
    let p1 = 0, p2 = 0, output = [];
    while (p1 < firstList.length && p2 < secondList.length) {
        if (firstList[p1][1] < secondList[p2][0]) {
			// Not Overlapping.
            // -----
            //       -----
            p1++;
        } else if (secondList[p2][1] < firstList[p1][0]) {
			// Not Overlapping.
            //       -----
            // -----
            p2++;
        } else {
		    // Overlapping
            let s = Math.max(firstList[p1][0], secondList[p2][0]);
            let e = Math.min(firstList[p1][1], secondList[p2][1]);
            output.push([s, e]);
            if (firstList[p1][1] <= secondList[p2][1]) {
                // -----
                //    -----
                // OR
                //   ---
                // -------
                p1++;
            } else {
                //    -----
                // -----
                // OR
                // -------
                //   ---
                p2++;
            }
        }
    }
    return output;
};

// TC = O(M+N)
// SC = O(1)