/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */

// Problem: https://leetcode.com/problems/top-k-frequent-elements/
// Ref: https://www.youtube.com/watch?v=YPTqKIgVk-k
// Use Bucket Sort
var topKFrequent = function (nums, k) {
    // The index of bucket Array is the frequency of num appears in nums
    // The value of bucket Array is the value in nums with coresponding to the frequency.
    // eg: 7 and 10 both appear in nums 3 times, then bucket[3] = [7,10]
    // Note: the maximum frequency is nums.length if all the num in nums are the same, therefore the maximum index of our bucket array length is just nums.length.
    let output = [];
    let bucket = new Array(nums.length + 1);
    let map = new Map();
    // Create hashmap ({val, freq}) for nums
    for (let i of nums) {
        if (map.has(i)) {
            map.set(i, map.get(i) + 1);
        } else {
            map.set(i, 1);
        };
    };
    // Loop into the map object, fill the buckey array in perticular index position for all the items in map object.
    for (let [val, freq] of map) {
        if (bucket[freq] === undefined) {
            bucket[freq] = [val];
        } else {
            bucket[freq].push(val);
        };
    };
    // Search the items from right to left in bucket array.
    let i = bucket.length - 1;
    while (i > 0 && k > 0) {
        if (bucket[i] !== undefined) {
            bucket[i].forEach((e) => output.push(e));
            k -= bucket[i].length;
        };
        i--;
    }
    return output;
};


// Solution 2: Hashmap(object)
var topKFrequent = function (nums, k) {
    const map = {};
    for (let i of nums) {
        if (map[i]) {
            map[i].value = map[i].value + 1;
        } else {
            map[i] = {
                key: i,
                value: 1
            };
        };
    };
    const res = Object.keys(map).map(i => map[i]).sort((a, b) => b.value - a.value);
    return res.slice(0, k).map(item => item.key);
};