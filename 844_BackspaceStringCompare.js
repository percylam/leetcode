/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */

// Problem: https://leetcode.com/problems/backspace-string-compare/
// Ref: https://www.youtube.com/watch?v=vgog1EuEJYQ

// Solve it in O(n) time and O(1) space
// Use Two-pointer
var backspaceCompare = function(s, t) {
    let sPointer = s.length - 1, tPointer = t.length - 1, sSkip = 0, tSkip = 0;
    while (sPointer >= 0 || tPointer >= 0) {
        while (sPointer >= 0) {
            if (s.charAt(sPointer) === "#") {
                sSkip++;
                sPointer--;
            } else if (sSkip > 0) {
                sSkip--;
                sPointer--;
            } else {
                break;
            }
        }
        
        while (tPointer >= 0) {
            if (t.charAt(tPointer) === "#") {
                tSkip++;
                tPointer--;
            } else if (tSkip > 0) {
                tSkip--;
                tPointer--;
            } else {
                break;
            }
        }
        
        if (s.charAt(sPointer) !== t.charAt(tPointer)) {
            return false;
        } else {
            sPointer--;
            tPointer--;
        }
    }
    return true
};


//************************************************************************************************************* */
// Solution 2: Two-Pointer & DP
var backspaceCompare = function(s, t) {
    let sPointer = s.length - 1, tPointer = t.length - 1, sSkip = 0, tSkip = 0;
    
    let check = (string, pointer, skip) => {
        while (pointer >= 0) {
            if (string.charAt(pointer) === "#") {
                skip++;
                pointer--;
            } else if (skip > 0) {
                skip--;
                pointer--;
            } else {
                break;
            }
        }
        return [pointer, skip];
    }
    
    while (sPointer >= 0 || tPointer >= 0) {
        let arrS = check(s, sPointer, sSkip), arrT = check(t, tPointer, tSkip);
        sPointer = arrS[0], sSkip = arrS[1], tPointer = arrT[0], tSkip = arrT[1];
        if (s.charAt(sPointer) !== t.charAt(tPointer)) {
            return false;
        } else {
            sPointer--;
            tPointer--;
        }
    }
    return true
};