/**
 * @param {string} s
 * @return {string}
 */
// Solution 1: ASCII table
 var toLowerCase = function(s) {
    let output = "";
    for (let i = 0; i < s.length; i++) {
        let code = s.charCodeAt(i);
        if (code > 64 && code < 91) {
            output += String.fromCharCode(code + 32);
        } else {
            output += s.charAt(i);
        }
    }
    return output;
};
// a~z: 97~122
// A~Z: 65~90


//================================================================================
// Solution 2: toLowerCase
var toLowerCase = function(s) {
    return s.toLowerCase(s)
}