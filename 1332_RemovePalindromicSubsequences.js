/**
 * @param {string} s
 * @return {number}
 */

// string contains 2 charaters (a OR b) only.
// Possible output ONLY are 0 (input string is empty), 1(input string is palindrome) and 2(input string is not palindrome)

 var removePalindromeSub = function(s) {
    if (s.length == 0) {
        return 0;
    }
    let left = 0;
    let right = s.length - 1;
    while (left < right) {
        if (s.charAt(left) != s.charAt(right)) {
            return 2;
        }
        left++;
        right--;
    }
    return 1;
};
// Expect output: 2
console.log(removePalindromeSub(s = "baabb"))