/**
 * @param {string} s
 * @return {string[]}
 */

// Problem: https://leetcode.com/problems/letter-case-permutation/
// Ref: https://www.youtube.com/watch?v=8ncNSmLVB4E&ab_channel=AndyGala

var letterCasePermutation = function (s) {
    /**
     * main(input):
     *  result = [];
     *  dfs(i, input, slate):
     *      // backtrack case (we don't have this case here.)
     * 
     *      // base case
     *      if (i == input.length) {
     *          result.push(slate.slice());
     *          return;
     *      };
     * 
     *      // dfs recursive case
     *      char = input[i]
     *      if (char is number) {
     *          slate.push(char);
     *          dfs(i+1, input, slate);
     *          slate.pop();
     *      } else {
     *          if (char is in upper case) {
     *              slate.push(char.toLowerCase());
     *              dfs(i+1, input, slate);
     *              slate.pop();
     *          } else {
     *              slate.push(char.toUpperCase());
     *              dfs(i+1, input, slate);
     *              slate.pop();
     *          };
     *      };    
     */

    let output = [];

    const dfs = (i, s, slate) => {
        // base case
        if (i === s.length) {
            output.push(slate.join(''));
            return;
        };

        // dfs recursive case
        let char = s[i];
        if (isNaN(char)) {
            // char is in uppercase
            slate.push(char.toLowerCase());
            dfs(i + 1, s, slate);
            slate.pop();

            // char is in uppercase
            slate.push(char.toUpperCase());
            dfs(i + 1, s, slate);
            slate.pop();
        } else {
            // char is a number.
            slate.push(char);
            dfs(i + 1, s, slate);
            slate.pop();
        };
    };

    dfs(0, s, [])
    return output;
};

console.log(letterCasePermutation(s = "a1b2cd"))