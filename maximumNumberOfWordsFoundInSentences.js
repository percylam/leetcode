var mostWordsFound = function (sentences) {
    let output = 0;
    for (let i of sentences) {
        let element = i.split(' ');
        if (element.length > output) {
            output = element.length
        };
    }
    return output
};

console.log(
    mostWordsFound(
        sentences = ["alice and bob love leetcode", "i think so too", "this is great thanks very much"]
    )
)