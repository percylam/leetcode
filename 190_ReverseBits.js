/**
 * @param {number} n - a positive integer
 * @return {number} - a positive integer
 */
 var reverseBits = function(n) {
    let output = 0;
    let pow = 31
    while (n > 0) {
        let rightMost = n & 1;
        output += (rightMost << pow);
        // here we must use '>>>' instead of '>>'.
        n = n >>> 1;
        pow--;
    }
    // here we must use '>>>' instead of '>>'.
    return output >>> 0
};

