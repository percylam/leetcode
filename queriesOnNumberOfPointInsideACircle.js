var countPoints = function(points, queries) {
    let output = [];
    for (let q = 0; q < queries.length; q++){
        let count = 0;
        for (let p = 0; p < points.length; p++){
            let disSqu = Math.pow((queries[q][0]-points[p][0]),2) + Math.pow((queries[q][1]-points[p][1]),2);
            let rediusSqu = Math.pow(queries[q][2],2);
            if (disSqu <= rediusSqu){
                count += 1
            }
        }
        output.push(count)
    }
    return output
};

// console.log(countPoints(points=[[1,3],[3,3],[5,3],[2,2]],queries=[[2,3,1],[4,3,1],[1,1,2]]))
console.log(countPoints(points = [[1,1],[2,2],[3,3],[4,4],[5,5]], queries = [[1,2,2],[2,2,2],[4,3,2],[4,3,3]]))