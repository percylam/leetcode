var countKDifference = function (nums, k) {
    let output = 0;
    let map = new Map();
    for (let i of nums) {
        if (map.get(i) === undefined) {
            map.set(i, 1);
        } else {
            let freq = map.get(i) + 1;
            map.set(i, freq);
            i = i.toString();
        }
    }
    for (let i of nums) {
        if (typeof i === "number") {
            let j = i + k;
            if (map.get(j) != undefined) {
                output += map.get(j)
            }
        }
    }
    return output;
};



// var countKDifference = function (nums, k) {
//     let output = 0;
//     while (nums.length != 0) {
//         let target = nums.shift();
//         for (let i of nums) {
//             if (i == target + k || target == i + k) {
//                 output += 1
//             }
//         }
//     }
//     return output;
// };


// console.log(countKDifference(nums = [1, 2, 2, 1], k = 1)) // output = 4
// console.log(countKDifference(nums = [1,3], k = 3)) // output = 0
console.log(countKDifference(nums = [3, 2, 1, 5, 4], k = 2)) // output = 3