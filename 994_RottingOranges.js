/**
 * @param {number[][]} grid
 * @return {number}
 */

/** Problem: https://leetcode.com/problems/rotting-oranges/
* Ref: https://www.youtube.com/watch?v=y704fEOx0s0&t=5s
* Use BFS.
* Step 1: Loop into the grid,
*         (1) use variable (fresh) to record total amount of fresh oranges.
*         (2) use queue to store the position ([row,col]) of rotted oranges in grid.
* Step 2: Use BFS to rote the oranges next to the bad oranges in queue by creating a rotten function. for each rote, the number of fresh should be reduced by 1. BUT the output number (time) should ONLY be accumated for each Queue Length CLEAR (in the while loop, we dequeue the entire queue. This counts 1 ONLY. (time += 1)).
* Step 3: Everytime we run the rotten function, new orange may be added into the queue, but it doesn't affect the Queue Length we used in the while loop in Step 2, since it was the queue length in previous minute.
* Step 4: Return the output if fresh == 0; Else, some good orange still in the grid and never be rotten while we need to return -1.
*/

var orangesRotting = function(grid) {
    let output = 0;
    let queue = [];
    let fresh = 0;
    for (let row = 0; row < grid.length; row++) {
        for (let col = 0; col < grid[0].length; col++) {
            if (grid[row][col] == 1) {
                fresh++;
            };
            if (grid[row][col] == 2) {
                queue.push([row, col]);
            };
        };
    };
    
    let rotten = (grid, row, col) => {
        if (row - 1 >= 0 && grid[row-1][col] == 1) {
            grid[row-1][col] = 2;
            fresh --;
            queue.push([row-1,col]);
        };
        if (row + 1 < grid.length && grid[row+1][col] == 1) {
            grid[row+1][col] = 2;
            fresh --;
            queue.push([row+1,col]);
        };
        if (col - 1 >= 0 && grid[row][col-1] == 1) {
            grid[row][col-1] = 2;
            fresh --;
            queue.push([row,col-1]);
        };
        if (col + 1 < grid[0].length && grid[row][col+1] == 1) {
            grid[row][col+1] = 2;
            fresh --;
            queue.push([row,col+1]);
        };
    };
    
    while (queue.length > 0 && fresh > 0) {
        let qLen = queue.length;
        for (let i = 0; i < qLen; i++) {
            let current = queue.shift();
            rotten(grid, current[0], current[1]);
        };
        output += 1;
    };
    if (fresh == 0) {
        return output;
    } else {
        return -1;
    };
};