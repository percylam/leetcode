/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
    if (nums.length == 0) {
        return 0;
    };
    let dp = new Array(nums.length + 1)
    dp[0] = 0;
    dp[1] = nums[0];

    for (let i = 1; i < nums.length; i++) {
        // dp array is the record of maximum sum in each house(nums[i]).
        dp[i+1] = Math.max(dp[i], dp[i-1] + nums[i])
    }
    return dp[nums.length]
};

console.log(rob(nums = [10, 1, 5, 100, 1])); 
// dp = [Math.max(0, 10), Math.max(10, 1), Math.max(1, 10+5), Math.max(15, 10+100), Math.max(110, 15+1)];