/**
 * @param {number} n
 * @return {number}
 */

// Problem: https://leetcode.com/problems/fibonacci-number/submissions/
// Recursion (Memorization usage).

 var fib = function(n) {
    if (n < 2) {
        return n;
    };
    return fib(n-2) + fib(n-1);
};