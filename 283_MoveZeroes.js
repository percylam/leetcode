// Problem: https://leetcode.com/problems/move-zeroes/
// Reference: https://www.youtube.com/watch?v=aayNRwUN3Do&ab_channel=NeetCode


/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
 var moveZeroes = function(nums) {
    let nonZero = 0;
    for (let i in nums) {
        if (nums[i] != 0) {
            nums[nonZero] = nums[i]; 
            nonZero++;
        }
    }
    nums.fill(0,nonZero,nums.length)
};