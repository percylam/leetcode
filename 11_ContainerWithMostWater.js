/**
 * @param {number[]} height
 * @return {number}
 */

// Two Pointer
var maxArea = function(height) {
    let output = 0;
    let left = 0;
    let right = height.length - 1;
    while (left < right) {
        let heightl = Math.min(height[left],height[right]);
        let width = right - left;
        if (output < heightl * width) {
            output = heightl * width;
        };
        if (height[left] <= height[right]) {
            left++;
        } else {
            right--;
        };
    };
    return output;
};