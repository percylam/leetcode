var checkStraightLine = function(coordinates) {
    let slope = (coordinates[0][1] - coordinates[1][1]) / (coordinates[0][0] - coordinates[1][0])
    console.log(slope)
    for (let i = 1; i < coordinates.length - 1; i++) {
        let point1 = coordinates[i];
        let point2 = coordinates[i + 1];
        // horizontal (y = y)
        if (slope == 0) {
            if (point1[1] != point2[1]) {
                return false
            }
        }
        // vertical (x = x)
        if (slope == "Infinity" || slope == "-Infinity") {
            if (point1[0] != point2[0]) {
                return false
            }
        }
        // console.log("look:",(point1[1]-point2[1])/(point1[0]-point2[0]))
        else if (((point1[1]-point2[1])/(point1[0]-point2[0])) != slope) {
            console.log("haha")
            return false
        }
    }
    return true
};

console.log(checkStraightLine(coordinates=[[0,0],[0,1],[0,-1]]))