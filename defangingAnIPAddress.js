/**
 * @param {string} address
 * @return {string}
 */
 var defangIPaddr = function(address) {
    let output = "";
    for (let i of address) {
        if (i == "."){
            output += "a"
        } else {
            output += i
        }
    }
    return output;
};

console.log(defangIPaddr("255.100.50.0"))