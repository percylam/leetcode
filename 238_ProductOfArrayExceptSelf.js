/**
 * @param {number[]} nums
 * @return {number[]}
 */

// Problem: https://leetcode.com/problems/product-of-array-except-self/
// Ref: https://www.youtube.com/watch?v=bNvIQI2wAjk

// Note: Must runs in O(n) time and without using the division operation. (Therefore we cannot calculate the accuminate product for nums.)

/** 
 * Basically, output[i] = (product of items which Before i) * (product of items which After i);
 * - So we loop from Left-to-Right for nums and use PREFIX to store the accuminative product of items Before i.
 * - And loop from Right-to-Left for nums and use POSTFIX to store the accuminative product of items after i.
 */

var productExceptSelf = function(nums) {
    let prefix = 1, postfix = 1, output = new Array(nums.length);
    for (let i in nums) {
        if (nums[i-1] === undefined) {
            nums[i-1] = 1;
        };
        output[i] = nums[i-1] * prefix;
        prefix = output[i];
    };
    for (let i = nums.length - 1; i >= 0; i--) {
        output[i] = output[i] * postfix;
        postfix = postfix * nums[i];
    };
    return output;
};

// Input = [-1,1,0,-3,3]; Expect = [0,0,9,0,0]
// Input= [1,2,3,4]; Expect = [24,12,8,6]