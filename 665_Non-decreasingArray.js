// Problem: https://leetcode.com/problems/non-decreasing-array/

/**
Loop into the nums array, if the value is NOT non-decressing (eg: nums = [1, 6, 3, 7]). The problem happens at 3 (nums[2]). We can fix it by either
(i) decressing 6 to 1 ~ 3 (refers to nums[0] and nums[2]), OR
(ii) incressing 3 to 6 ~ 7. (refers to nums[1] and nums[3]).

Here you may realise for the case (nums = [1, 6, 3, 5]), we can only fix it by decressing 6 to 1 ~ 3 (refers to nums[0] and nums[2]) SINCE 5 is the upper boundry for 3.

We can create 2 adjustcent pointers (nums[i] and nums[i+1]), the value ranges for them are
nums[i-1] <= nums[i] <= nums[i+1], AND
nums[i] <= nums[i+1] <= nums[i+2].

The last thing we should consider is the boundry for the loop (when i = 0 and i = nums.length - 1). Here I create 2 dummy points -100,000 (min value) for nums[i-1] and 100,000 (max value) for nums[i+2] when they are at the edges (null value).

Last but not least, a is to monitor whether the array has been changed before.
*/

/**
 * @param {number[]} nums
 * @return {boolean}
 */

 var checkPossibility = function(nums) {
    let a = 0;
    for (let i = 0; i < nums.length - 1; i++) {
        if(nums[i] > nums[i+1]) {
            if ((nums[i-1] || -100000) <= nums[i+1]) {
                nums[i] = nums[i-1] || -100000;
            } else if ((nums[i+2] || 100000) >= nums[i]){
                nums[i+1] = nums[i];
            } else {
                return false;
            };
            a++;
        };
        if (a > 1) {
            return false;
        }
    }
    return true;
};