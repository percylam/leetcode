/**
 * @param {number[]} nums
 * @return {number}
 */

// Question: https://leetcode.com/problems/jump-game-ii/

// BFS
var jump = function(nums) {
    let position = 0, output = 0;

    let bfs = (nums, position) => {
        let i = 0;
        let best = 0;
        let bestPo = position;
        while (i <= nums[position]) {
            if (nums[position] >= best) {
                best = nums[position];
                bestPo = i + position;
            };
            i++;
            best--;
        }
        return bestPo;
    };

    while (position < nums.length) {
        position = bfs(nums, position);
        output++;
    };

    return output;
};