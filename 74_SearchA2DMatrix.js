/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */

// The length of each row are the same since it is matrix!

/** Solution 1: Binary Search (twice)
 * Time Complexity: O(log m) + O(log n) = O(log(m * n))
 * Space Complexity: O(1)
 */
var searchMatrix = function (matrix, target) {
    let lastArr = matrix[matrix.length - 1];
    let max = lastArr[lastArr.length - 1], min = matrix[0][0];
    if (target < min || target > max) {
        return false;
    };

    const bs = (arr, target) => {
        let left = 0, right = arr.length - 1;
        while (left < right) {
            let mid = Math.floor((left + right) / 2);
            if (target == arr[mid]) {
                return true;
            } else if (target > arr[mid]) {
                left = mid + 1;
            } else {
                right = mid - 1;
            };
        };
        return target == arr[left];
    };

    if (target >= lastArr[0]) {
        return bs(lastArr, target);
    } else {
        let l = 0, r = matrix.length - 1;
        while (l < r) {
            let m = Math.floor((l + r) / 2);
            if (target == matrix[m][0]) {
                return true;
            } else if (target < matrix[m][0]) {
                // here cannot use r=m-1 because we only check into matrix[m] in else case(line 42 ~ 48). r=m-1 will directly skip the possible array m and jump into array m-1.
                r = m;
            } else {
                let last = matrix[m].length - 1;
                if (target <= matrix[m][last]) {
                    return bs(matrix[m], target);
                } else {
                    l = m + 1;
                };
            };
        };
        return false;
    };
};

//==================================================================================================================
// Solution 2: Treat it as 1D-array. Then use Binary Search.
// left = 0, right = row * col - 1, row = Math.floor(mid / m), col = mid % m;

var searchMatrix = function (matrix, target) {
    let n = matrix.length, m = matrix[0].length;
    let left = 0, right = m * n - 1;
    while (left < right) {
        let mid = Math.floor((left + right) / 2), midRow = Math.floor(mid / m);
        if (matrix[midRow][mid % m] < target) {
            left = mid + 1;
        } else {
            right = mid;
        };
    };
    //left == right;
    let row = Math.floor(left / m);
    return matrix[row][left % m] == target;
};



console.log(searchMatrix(matrix = [[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 60], [70, 71, 80], [81, 82, 83, 84, 85, 86, 88, 100]], target = 11))
// console.log(searchMatrix(matrix = [[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 60], [70, 71, 80], [81, 82, 83, 84, 85, 86, 88, 100]], target = 1))
