/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */

/**
 * Problem: https://leetcode.com/problems/binary-tree-inorder-traversal/
 * Ref: https://www.youtube.com/watch?v=QxFOR8sQuB4&t=305s&ab_channel=FisherCoder
 * 
 * Input = [1,2,3,4,5,null,null,null,null,6,7,null,null,null,8]
 * Output = [4,2,6,5,7,8,1,3]
 * Use DFS.
 * Principle: Left -> Node -> Right
 */


// // Not working.
//  var inorderTraversal = function(root) {
//     let output = [];
//     if (root == null) {
//         return output;
//     };
//     let stack = [root.val];
//     while (stack.length == 0) {
//         return output;
//     };

//     const dp = (node, stack, output) => {
//         // visit the very left node.
//         while (node.left !== null) {
//             node = node.left;
//             console.log(node);
//             console.log(stack);
//             stack.push(node.val);
//         };
//         output.push(stack.pop());
//         node = stack.pop();
//         output.push(node.val);
//         if (node.right !== null) {
//             node = node.right;
//             stack.push(node.val);
//             dp(node, stack, output);
//         } else {
//             output.push(node);
//             node = stack.pop();
//         };
//     };

//     dp(root, stack, output);
// };

// console.log(inorderTraversal(root=[1,2,3,4,5,null,null,null,null,6,7,null,null,null,8]))

var inorderTraversal = (root)=>{
    let stack=[];
    let visited = [];
    while(root || stack.length>0){
        while(root){
            stack.push(root);
            root = root.left
        }
        root = stack.pop();
        visited.push(root.val);
        root = root.right;
    }
    return visited
}