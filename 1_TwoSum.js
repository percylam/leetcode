// Question: 
// Input: nums = [2,7,11,15], target = 9
// Output: [0,1]
// Output: Because nums[0] + nums[1] == 9, we return [0, 1].
//(Constraints:)
// 2 <= nums.length <= 104
// -109 <= nums[i] <= 109
// -109 <= target <= 109
// Only one valid answer exists.

var twoSum = function (nums, target) {
    const map = new Map();
    // const hashTable = {}
    for (let i in nums) {
        const currentNumber = nums[i]
        const neededValue = target - currentNumber
        // const neededValueIndex = hashTable[neededValue]
        if (map.has(neededValue)) {
            return [map.get(neededValue), i]
        } else {
            map.set(currentNumber, i)
            // hashTable[currentNumber] = i
        }
    }
}

console.log(twoSum(nums = [2, 7, 11, 15], target = 9))

// O(n)

// nums = [2,7,11,15], target = 9


// Notes:
// Since we don't want to do it by using 2 cascaded for-loop(n*n). This method provides single-way for loop (loop once only).
// (1) Create an object to record the numbers' value and their index which we have tried BUT not fits the requirement.
//      hashTable = {value:index}
// (2) Calculate the required value for each number
//      neededValue = target - nums[i]
// (3) Use the neededValue as the key to get the value from our object hasTable. If the key exist, that means the neededValue has been looped before. Its index should be able to obtain by using the value as searching key in hashTable object.
// In an array, we can only find the value with index number directly, we never know its value and we need to use for loop to check whether the value exists in the array.
// But in Object, we can easily get the value by using a specific key. Especially when we use value as the key in object. We can easily call for its index.



var twoSum = function (nums, target) {
    const map = new Map();
    for (let i in nums) {
        const neededValue = target - nums[i];
        if (map.has(neededValue)) {
            return (map.get(neededValue), i)
        } else {
            map.set(nums[i], i);
        }
    }
};
