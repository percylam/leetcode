/**
 * Problem:
 * https://leetcode.com/problems/binary-search/
 * 
 * Reference Video:
 * https://www.youtube.com/watch?v=6ysjqCUv3K4
 */


/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */

// Solution 1:
var search = function (nums, target) {
    let left = 0;
    let right = nums.length - 1;
    while (right - left > 1) {
        let mid = Math.floor((left + right) / 2);
        if (nums[mid] == target) {
            return mid;
        } else if (nums[mid] > target) {
            right = mid;
        } else {
            left = mid;
        }
    }
    if (nums[left] == target) {
        return left;
    } else if (nums[right] == target) {
        return right;
    } else {
        return -1
    }
};

// Solution 2:
// var search = function(nums, target) {
//     let left = 0;
//     let right = nums.length - 1;
//     while (right >= left) {
//         let mid = Math.floor((left + right) / 2);
//         if (nums[mid] == target) {
//             return mid;
//         } else if (nums[mid] > target) {
//             right = mid - 1;
//         } else {
//             left = mid + 1;
//         }
//     }
//     return -1
// };

console.log(search(nums = [-1, 0, 3, 5, 9, 12], target = 9))