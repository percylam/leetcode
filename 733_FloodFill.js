/**
 * @param {number[][]} image
 * @param {number} sr
 * @param {number} sc
 * @param {number} newColor
 * @return {number[][]}
 */

// This problem can be solve by DFS approach. We use recursion to implement the DFS approach.

var floodFill = function(image, sr, sc, newColor) {
    // Do nothing if the newColor already in the start pixel.
    if (image[sr][sc] == newColor) {
        return image;
    };
    let oldColor = image[sr][sc];
    // DFS
    const fill = (image, sr, sc, oldColor, newColor) => {
        // We do nothing for these bad cases.
        if (sr >= image.length || sc >= image[0].length || sr < 0 || sc <0 || image[sr][sc] != oldColor) {
            return;
        };
        // Fill the newColor to the start pixel.
        image[sr][sc] = newColor;
        fill(image, sr+1, sc, oldColor, newColor);
        fill(image, sr-1, sc, oldColor, newColor);
        fill(image, sr, sc+1, oldColor, newColor);
        fill(image, sr, sc-1, oldColor, newColor);
    };
    //-----------------------------------
    fill(image, sr, sc, oldColor, newColor);
    return image;
};