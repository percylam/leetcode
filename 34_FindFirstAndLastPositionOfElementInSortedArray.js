/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */

// Problem: https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/
// You must write an algorithm with O(log n) runtime complexity.

// Binary Search
// TC = O(2log(N)) = O(log(N))
// SC = O(1)
var searchRange = function(nums, target) {
    // Search for first position
    let l = 0, r = nums.length - 1, left;
    while (l <= r) {
        let m = Math.floor((l + r) / 2);
        if (nums[m] < target) {
            l = m + 1;
        } else {
            r = m - 1;
        };
    };
    if (nums[l] !== target) {
        return [-1, -1];
    } else {
        left = l;
    }
    // Search for seconde position
    l = 0, r = nums.length - 1;
    while (l <= r) {
        console.log(r)
        let m = Math.floor((l + r) / 2);
        if (nums[m] <= target) {
            l = m + 1;
        } else {
            r = m - 1;
        };
    };
    return [left, r];
};
console.log(searchRange(nums = [1, 2, 3, 5, 7, 7, 8, 8, 8, 10], target = 8))