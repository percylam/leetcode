/**
 * @param {number[][]} triangle
 * @return {number}
 */

// Since the minimum path involve multi layer of the array (2D-array), we cannot decide which path is the minimum sum path while only compare 2 numbers from the top to bottom.
// But we can travel from bottom, since each parent note can only reach 2 neithber children. We can accuminate the sum from children to their parent. Select the minimum child for each parent. The accumination sum represents its child path minimum sum (like a binary tree).
// We can design a Dynamic Programing (dp) and recusively comparing the minimum child and calculate its path sum..

var minimumTotal = function(triangle) {
    let row = triangle.length;
    let ram = new Array(triangle.length + 1);
    ram.fill(0);
    let dp = (ram, row, triangle) => {
        for (let i = 0; i < ram.length - 1; i++) {
            ram[i] = Math.min(ram[i], ram[i+1]) + triangle[row-1][i];
        }
        ram.pop();
        return ram;
    }
    while (row > 0) {
        dp(ram, row, triangle);
        row--;
    }
    return ram[0];
};

