// Problem: https://leetcode.com/problems/reverse-string/

/**
 * @param {character[]} s
 * @return {void} Do not return anything, modify s in-place instead.
 */

 var reverseString = function(s) {
    let head = 0;
    let tail = s.length - 1;
    while (tail > head) {
        let temp = s[head];
        s[head] = s[tail];
        s[tail] = temp;
        head++;
        tail--;
    }
};