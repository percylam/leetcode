var containsDuplicate = function(nums) {
    // nums.sort();
    // var ref = "a";
    let obj = {};
    // console.log("123",1 in obj);
    for (let i = 0; i < nums.length; i++) {
        if (nums[i] in obj) {
            return true
        } else {
            let key = nums[i].toString();
            obj[key] = 1;
        }
    }
    return false
};


//===============================================================================================
// Solution 2: Map()
var containsDuplicate = function(nums) {
    let map = new Map();
    for (let i of nums) {
        if (map.has(i)) {
            return true;
        };
        map.set(i,0);
    };
    return false;
};

console.log(containsDuplicate(nums = [1,2,3]))