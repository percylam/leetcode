/**
 * @param {number[]} nums
 * @return {number}
 */

// Use BS
// Solution 1:
var findMin = function(nums) {
    let l = 0, r = nums.length - 1; 
    while (l + 1 < r) {
        if (nums[l] < nums[r]) {
            // array is sorted
            return nums[l];
        };
        let m = Math.floor((l + r) / 2);
        if (nums[m] > nums[l]) {
            // left-protion is sorted, we keep right-protion
            l = m;
        } else {
            r = m;
        };
    };
    return Math.min(nums[l],nums[r])
};

//********************************************************************************************** */
// Solution 2:
var findMin = function(nums) {
    let l = 0, r = nums.length - 1; 
    while (l < r) {
        let m = Math.floor((l + r) / 2);
        if (nums[m] > nums[r]) {
            // left-protion is sorted, we keep right-protion
            l = m + 1;
        } else {
            r = m;
        };
    };
    return nums[l]
};

// [3,4,5,1,2] Pass
// [4,5,6,7,0,1,2] Pass
// [11,13,15,17] Pass
// [2,1] Pass
// [4,5,1,2,3] Pass
// [3,1,2] Pass