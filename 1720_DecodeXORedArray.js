/**
 * @param {number[]} encoded
 * @param {number} first
 * @return {number[]}
 */
var decode = function (encoded, first) {
    let output = [first];
    // encoded ^ first = second
    let i = 0;
    while (output.length < encoded.length + 1) {
        output.push(encoded[i] ^ output[output.length - 1]);
        i++;
    }
    return output;
};
// 0 => 00
// 1 => 01
// 2 => 10
// 3 => 11
// 0 ^ 1 => 1 (1)