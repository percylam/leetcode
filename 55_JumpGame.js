/**
 * @param {number[]} nums
 * @return {boolean}
 */

/** 
 * Problem: https://leetcode.com/problems/jump-game/
 * Ref: https://www.youtube.com/watch?v=Yan0cv2cLy8&ab_channel=NeetCode
*/ 

/**
 * We can check from the end position (nums.length - 1), and shift the goal to the left if it can reach the end position (nums.length - 1) from its left position (nums.length - 2).
 * How to check whether it can reach?
 * i + nums[i] >= goal means it can reach to the next position.
 */

 var canJump = function(nums) {
    let goal = nums.length - 1;
    for (i = nums.length - 1; i >= 0; i--) {
        if (i + nums[i] >= goal) {
            goal = i;
        };
    };
    return goal == 0;
};