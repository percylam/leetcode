/**
 * @param {number[]} nums
 * @return {number[]}
 */
var runningSum = function(nums) {
    let output = [];
    let sum = 0;
    const length = nums.length;
    for (let j = 0; j < length; j++) {
        for (let i of nums) {
            sum += i
        }
        output.unshift(sum);
        nums.pop();
        sum = 0
    }
    return output
};


// Solution 2: modify the nums directly.
var runningSum = function(nums) {
    let i = 0;
    let sum = 0;
    while (i < nums.length) {
        nums[i] = sum + nums[i];
        sum = nums[i];
        i++;
    };
    return nums;
};


console.log(runningSum([1,1,1,1]))