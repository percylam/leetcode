var maxProfit = function(prices) {
    let output = 0;
    for (let i = 0; i < prices.length; i++){
        let diff = prices[i+1] - prices[i];
        diff>0 ? output+=diff : output;
    }
    return output
};
console.log(maxProfit([0,1,10]))