/**
 * @param {number[]} nums
 * @return {number}
 */

// return index of the peak element
var findPeakElement = function(nums) {
    let l = 0, r = nums.length - 1;
    while (l < r) {
        let m = Math.floor((l + r) / 2);
        if (nums[m] < nums[m+1]) {
            l = m + 1;
        } else {
            r = m;
        };
    };
    return l;
};

// [6,5,4,3,2,3,2]
// [1,2,3,1]
// [1,2,1,3,5,6,4]