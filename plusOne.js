var plusOne = function(digits) {
    let index = digits.length - 1;
    digits[index] += 1;
    while (digits[index] >= 10) {
        digits[index] = 0;
        if (index == 0) {
            digits.unshift(1)
        } else {
            digits[index - 1] += 1;
            index -= 1;
        } 
    }
    return digits
};